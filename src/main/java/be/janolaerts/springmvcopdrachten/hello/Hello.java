package be.janolaerts.springmvcopdrachten.hello;

public interface Hello {

    public String sayHello();
}
