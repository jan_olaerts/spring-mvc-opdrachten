package be.janolaerts.springmvcopdrachten.repository;

import be.janolaerts.springmvcopdrachten.command.Book;

import java.util.List;

public interface BookRepository {

    public Book getBook(String isbn);
    public List<Book> getBooks();
}