package be.janolaerts.springmvcopdrachten.repository;

import be.janolaerts.springmvcopdrachten.command.Book;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BookRepositoryMemoryImpl implements BookRepository {

    private Map<String, Book> books  = new HashMap<>(Map.of(
            "9780156032971", new Book("Foucault's Pendulum", "Umberto Eco", "9780156032971", 14.41),
            "9780385504225", new Book("The Lost Symbol", "Dan Brown", "9780385504225", 19.98),
            "0545139708", new Book("Harry Potter and the Deathly Hallows", "J.K. Rowling", "0545139708", 15d)));

    @Override
    public Book getBook(String isbn) {
        if(isbn == null) throw new IllegalArgumentException();

        return books.getOrDefault(isbn, null);
    }

    @Override
    public List<Book> getBooks() {

        List<Book> bookObjects = new ArrayList<>();
        for(Map.Entry<String, Book> entry : books.entrySet()) {
            bookObjects.add(entry.getValue());
        }

        return bookObjects;
    }
}