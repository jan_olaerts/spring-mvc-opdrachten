package be.janolaerts.springmvcopdrachten.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/sum")
public class SumController {

    @GetMapping
    public String showSumPage() {
        return "numberSum";
    }

    @PostMapping
    public ModelAndView getSum(@RequestParam("d1") String d1, @RequestParam("d2") String d2) {
        int intD1 = Integer.parseInt(d1);
        int intD2 = Integer.parseInt(d2);
        int result = intD1 + intD2;
        return new ModelAndView("numberSum", "result", result);
    }
}