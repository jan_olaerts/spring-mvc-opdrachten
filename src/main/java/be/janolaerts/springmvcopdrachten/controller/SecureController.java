package be.janolaerts.springmvcopdrachten.controller;

import be.janolaerts.springmvcopdrachten.service.SecureBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/secured")
public class SecureController {

    @Autowired
    private SecureBean secureBean;

    @GetMapping
    public String getSecret(ModelMap model) {

        model.addAttribute("secureMessage", secureBean.getSecret());
        return "secured";
    }
}