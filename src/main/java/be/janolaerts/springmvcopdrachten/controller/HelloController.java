package be.janolaerts.springmvcopdrachten.controller;

import be.janolaerts.springmvcopdrachten.hello.Hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Controller
//@Scope("prototype")
public class HelloController {

    private Hello helloService;

    @Autowired
    public void setHelloService(Hello helloService) {
        this.helloService = helloService;
    }

    @RequestMapping("/hello")
    public ModelAndView handleHello() {
        String text = helloService.sayHello();
        return new ModelAndView("helloView", "message", text);
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Starting helloController");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Stopping helloController");
    }
}