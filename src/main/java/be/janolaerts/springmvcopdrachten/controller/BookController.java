package be.janolaerts.springmvcopdrachten.controller;

import be.janolaerts.springmvcopdrachten.command.Book;
import be.janolaerts.springmvcopdrachten.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @GetMapping
    public String showBooksView(ModelMap model) {
        List<Book> books = bookRepository.getBooks();
        model.addAttribute("books", books);
        return "books/booklist";
    }

    @GetMapping(params="isbn")
    public String showBookDetailsView(ModelMap model, @RequestParam String isbn) {

        model.addAttribute("book", bookRepository.getBook(isbn));
        return "books/bookdetail";
    }
}