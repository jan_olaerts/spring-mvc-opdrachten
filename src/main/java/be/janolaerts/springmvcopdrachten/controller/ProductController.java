package be.janolaerts.springmvcopdrachten.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/products")
@SessionScope
public class ProductController {

    private List<String> products = new ArrayList<>();

    @ModelAttribute("products")
    public List<String> getProducts() {
        return products;
    }

    @GetMapping
    public String showProductPage(ModelMap model) {
//        model.addAttribute("products", products);
        return "products";
    }

    @PostMapping
    public String handleProducts(@RequestParam Map<String, String> params) {

        for(Map.Entry<String, String> product : params.entrySet()) {
            products.add(product.getValue());
        }

        return "redirect:products";
    }

    @PostMapping(params="action=Del")
    public String handleDelete(@RequestParam Map<String, String> params) {

        int index = Integer.parseInt(params.get("index"));
        products.remove(index);

        return "redirect:products";
    }

    @PostMapping(params="action=Change")
    public String handleChange(@RequestParam Map<String, String> params) {

        int index = Integer.parseInt(params.get("index"));
        String updatedProduct = params.get("product");
        products.set(index, updatedProduct);

        return "redirect:products";
    }
}