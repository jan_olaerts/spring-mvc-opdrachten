package be.janolaerts.springmvcopdrachten.controller;

import be.janolaerts.springmvcopdrachten.command.Vehicle;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("/vehicle")
@RequestScope
public class VehicleController {

    @ModelAttribute("optionsList")
    public String[] getOptionsList() {
        return new String[] { "Alarm", "Handsfree carkit", "MP3 player", "Sport pack" };
    }

    @GetMapping
    public String showVehicleForm(@ModelAttribute("vehicle") Vehicle vehicle) {
        return "vehicles/vehicleForm";
    }

    @PostMapping
    public String handleRegistrationForm(@Valid @ModelAttribute("vehicle") Vehicle vehicle, BindingResult br, ModelMap model) {

        if(br.hasErrors()) {

            for(String code : br.getFieldError().getCodes()) {
                System.out.println(code);
            }

            System.out.println("error");
            return "vehicles/vehicleForm";
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        model.addAttribute("deis", vehicle.getEntryIntoService().format(formatter));
        return "vehicles/vehicleConfirm";
    }
}