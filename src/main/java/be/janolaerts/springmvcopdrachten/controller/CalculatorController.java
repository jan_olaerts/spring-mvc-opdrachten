package be.janolaerts.springmvcopdrachten.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.ApplicationScope;

import java.util.Map;

@Controller
@RequestMapping("/calculator")
@Scope("session")
@SessionAttributes("result")
public class CalculatorController {

    @GetMapping
    public String showCalculatorView(ModelMap model) {
        if(model.get("result") == null)
            model.addAttribute("result", 0d);

        return "calculator";
    }

    @PostMapping
    public String calculate(@RequestParam("number") Double number,
                            @RequestParam("operation") String operation,
                            ModelMap model) {

        double result = (double) model.get("result");
        if(number == null && operation.matches("[*|/]")) number = 1d;
        if(number == null && operation.matches("[+|-]")) number = 0d;

        switch(operation) {
            case "+":
                result += number;
                break;
            case "-":
                result -= number;
                break;
            case "*":
                result *= number;
                break;
            case "/":
                result /= number;
                break;
            default:
                result = 0;
        }

        model.addAttribute("result", result);
        return "redirect:calculator";
    }
}