package be.janolaerts.springmvcopdrachten.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Map;

@Controller
@RequestMapping("/age")
@RequestScope
public class AgeController {

    @GetMapping
    public String handleAge() {
        return "ages/selectAge";
    }

    @PostMapping
    public String handleSelection(@RequestParam Map<String, String> params) {

        if(params.containsValue("Cancel"))
            return "redirect:age?action=cancel";

        for(Map.Entry<String, String> entry : params.entrySet()) {

            String key = entry.getKey();
            String value = entry.getValue();

            System.out.println(key + "=" + value);

            return "redirect:age?" + key + "=" + value;
        }

        return "redirect:";
    }

    @GetMapping(params="age=0-9")
    public String handleChildren() {
        return "ages/children";
    }

    @GetMapping(params="age=10-19")
    public String handleTeenagers() {
        return "ages/teenagers";
    }

    @GetMapping(params="age=>20")
    public String handleAdults() {
        return "ages/adults";
    }

    @GetMapping(params="action=cancel")
    public String handleCancel() {
        return "ages/cancel";
    }
}