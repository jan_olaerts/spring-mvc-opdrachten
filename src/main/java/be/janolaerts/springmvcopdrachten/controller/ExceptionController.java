package be.janolaerts.springmvcopdrachten.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExceptionController {

//    @RequestMapping("/")
    public String handleRequest() {

        try {

        } catch(Exception ex) {
            return "exception";
        }

        return "ok";
    }
}