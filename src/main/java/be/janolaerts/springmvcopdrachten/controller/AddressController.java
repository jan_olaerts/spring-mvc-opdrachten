package be.janolaerts.springmvcopdrachten.controller;

import be.janolaerts.springmvcopdrachten.command.Address;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/address")
public class AddressController {

    @GetMapping
    public String showInputAddressView() {
        return "address/inputAddress";
    }

    @PostMapping
    public String showOutputAddressView(Address address) {
        return "address/outputAddress";
    }
}