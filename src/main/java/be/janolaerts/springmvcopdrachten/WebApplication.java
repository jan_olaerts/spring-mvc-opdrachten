package be.janolaerts.springmvcopdrachten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled=true)
public class WebApplication {

    @Bean
    public WebSecurityConfigurer<WebSecurity> securityConfigurer() {

        return new WebSecurityConfigurerAdapter() {

            @Override
            protected void configure(AuthenticationManagerBuilder auth) throws Exception {

                BCryptPasswordEncoder bc = new BCryptPasswordEncoder();

                auth.inMemoryAuthentication()
                        .passwordEncoder(bc)
                        .withUser("homer")
                        .password(bc.encode("password"))
                        .roles("ADULT")
                        .and()
                        .withUser("bart")
                        .password(bc.encode("password"))
                        .roles("MINOR");
            }

            @Override
            protected void configure(HttpSecurity http) throws Exception {

                http.httpBasic();
                http.csrf().disable();
                http.authorizeRequests()
                        .antMatchers("/secured/**")
                        .hasAnyRole("ADULT", "MINOR");

                http.formLogin()
                        .loginPage("/login")
                        .permitAll();
            }
        };
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {

        LocaleChangeInterceptor interceptor =
                new LocaleChangeInterceptor();

        interceptor.setParamName("language");
        return interceptor;
    }

    @Bean
    public LocaleResolver localeResolver() {

        CookieLocaleResolver resolver = new CookieLocaleResolver();
        resolver.setCookieMaxAge(1000);
        return resolver;
    }

    @Bean
    public WebMvcConfigurer configurer() {

        return new WebMvcConfigurer() {

            @Override
            public void addViewControllers(
                    ViewControllerRegistry registry) {

                registry.addViewController("/language")
                        .setViewName("language");

                registry.addViewController("/login") // Connected to formLogin
                        .setViewName("login");
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(localeChangeInterceptor())
                .addPathPatterns("/"); // Path where Spring looks for language param
            }
        };
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(WebApplication.class, args);
    }
}