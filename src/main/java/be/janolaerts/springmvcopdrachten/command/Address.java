package be.janolaerts.springmvcopdrachten.command;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Address implements Serializable {

    private String firstName;
    private String lastName;
    private String streetNumber;
    private String zipCode;
    private String city;
    private String country;

    public Address() {
    }

    public Address(String firstName, String lastName, String streetNumber, String zipCode, String city, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }
}