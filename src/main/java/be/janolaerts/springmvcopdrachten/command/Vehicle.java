package be.janolaerts.springmvcopdrachten.command;

import be.janolaerts.springmvcopdrachten.vehicleenum.FuelType;
import be.janolaerts.springmvcopdrachten.vehicleenum.TransmissionType;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;

@Component
public class Vehicle implements Serializable {

    @NotEmpty
    private String brand;

    @NotEmpty
    private String type;

    @NotNull
    private FuelType fuel;

    @Min(1971)
    private int year;

    @Min(1)
    private int power;

    @NotNull
    private TransmissionType transmissionType;

    @PastOrPresent
    @DateTimeFormat(pattern="dd/MM/yyyy")
    @NotNull
    private LocalDate entryIntoService;

    @Pattern(regexp="^[\\d]-[A-Z]{3}-[\\d]{3}$")
    private String plate;

//    @Digits(integer = 1000, fraction = 2)
    @NumberFormat(style = NumberFormat.Style.CURRENCY, pattern = "#,###.##")
    private float listPrice;

    private boolean towBar;

    private String[] options;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FuelType getFuel() {
        return fuel;
    }

    public void setFuel(FuelType fuel) {
        this.fuel = fuel;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public TransmissionType getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(TransmissionType transmissionType) {
        this.transmissionType = transmissionType;
    }

    public LocalDate getEntryIntoService() {
        return entryIntoService;
    }

    public void setEntryIntoService(LocalDate entryIntoService) {
        this.entryIntoService = entryIntoService;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public float getListPrice() {
        return listPrice;
    }

    public void setListPrice(float listPrice) {
        this.listPrice = listPrice;
    }

    public boolean isTowBar() {
        return towBar;
    }

    public void setTowBar(boolean towBar) {
        this.towBar = towBar;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "brand='" + brand + '\'' +
                ", type='" + type + '\'' +
                ", fuel=" + fuel +
                ", year=" + year +
                ", power=" + power +
                ", transmissionType=" + transmissionType +
                ", entryIntoService=" + entryIntoService +
                ", plate='" + plate + '\'' +
                ", listPrice=" + listPrice +
                ", towBar=" + towBar +
                ", options=" + Arrays.toString(options) +
                '}';
    }
}