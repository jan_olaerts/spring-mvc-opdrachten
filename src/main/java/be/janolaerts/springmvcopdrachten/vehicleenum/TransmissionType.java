package be.janolaerts.springmvcopdrachten.vehicleenum;

public enum TransmissionType {

    MANUAL, AUTOMATIC
}