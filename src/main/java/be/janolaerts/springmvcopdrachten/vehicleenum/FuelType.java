package be.janolaerts.springmvcopdrachten.vehicleenum;

public enum FuelType {

    GASOLINE, DIESEL
}