package be.janolaerts.springmvcopdrachten.service;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

@Service
public class SecureBeanImpl implements SecureBean {

    @Override
    @Secured("ROLE_ADULT")
    public String getSecret() {
        return "This is a secured message";
    }
}