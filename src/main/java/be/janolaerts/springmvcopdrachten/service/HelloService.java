package be.janolaerts.springmvcopdrachten.service;

import be.janolaerts.springmvcopdrachten.hello.Hello;
import org.springframework.stereotype.Service;

@Service("helloService")
public class HelloService implements Hello {

    @Override
    public String sayHello() {
        return "Hello World";
    }
}