package be.janolaerts.springmvcopdrachten.service;

public interface SecureBean {

    String getSecret();
}